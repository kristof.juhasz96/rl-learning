import numpy as np
from rl_learning.common.environment import State, Action, Environment


class CliffWalking(Environment):

    def __init__(self, length: int = 20, width: int = 0.4):

        self.env_name = "Cliff Walking"
        self.length = length
        self.width = width
        super().__init__()

    def transition_reward_function(self, state: State, action: Action) -> (State, float):

        moves = {'u': [0, 1], 'd': [0, -1], 'r': [1, 0], 'l': [-1, 0]}

        next_state_value = list(np.array(state.name) + np.array(action.value))
        next_state_id = next_state_value[1] * self.length + next_state_value[0]
        if(0 > next_state_value[0] or 0 > next_state_value[1] or next_state_value[0] >= self.length or next_state_value[1] >= self.width):
            next_state_value = state.name
            next_state_id = state.id

        reward = -1
        is_terminal = False
        if(next_state_value[1] == 0 and next_state_value[0] > 0):
            is_terminal = True
            if(next_state_value[0] < self.width - 1):
                reward = -100

        next_state = State(next_state_value, int(next_state_id), is_terminal)
        actions = []
        if not is_terminal:
            for name in ['u', 'd', 'r', 'l']:
                action = Action(name, moves[name])
                actions.append(action)
        next_state.add_allowed_actions(actions)

        return next_state, reward

    def init_environment(self):

        actions = []
        moves = {'u' : [0,1], 'd' : [0, -1], 'r' : [1, 0], 'l' : [-1, 0]}
        for name in ['u', 'd', 'r', 'l']:
            action = Action(name, moves[name])
            actions.append(action)

        start_state = State([0, 0], 0, False)
        start_state.add_allowed_actions(actions)
        self.add_state(start_state)
        n = 1
        for i in range(1, self.length):
            state = State([i, 0], n, True)
            self.add_state(state)
            n = n + 1
        for i in range(0, self.length):
            for j in range(1, self.width):
                state = State([i, j], n, False)
                state.add_allowed_actions(actions)
                self.add_state(state)
                n = n + 1

