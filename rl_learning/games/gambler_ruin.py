import random

from rl_learning.common.environment import State, Action, Environment


class GamblerRuinGame(Environment):

    def __init__(self, final_state=20, p_win=0.4):

        self.env_name = "Gambler Ruin"
        self.final_state_value = final_state
        self.p_win = p_win
        super().__init__()

    def transition_reward_function(self, state: State, action: Action) -> (State, float):
        p_win = self.p_win
        result = random.random()
        if result < p_win:

            next_state_value = state.name + action.value
        else:
            next_state_value = state.name - action.value

        reward = 0
        is_terminal = False
        if next_state_value == self.final_state_value:
            reward = 1
            is_terminal = True
        elif next_state_value == 0:
            is_terminal = True

        next_state = State(next_state_value, is_terminal)
        allowed_actions = []
        if not is_terminal:
            for a in range(1, min(next_state_value, self.final_state_value - next_state_value) + 1):
                action = Action(a, a)
                allowed_actions.append(action)
        next_state.add_allowed_actions(allowed_actions)
        return next_state, reward

    def init_environment(self):
        final_state = self.final_state_value
        broke = State(0, True)
        won = State(final_state, True)

        for i in range(1, final_state):
            state = State(i, False)
            allowed_actions = []
            for a in range(1, min(i, final_state - i) + 1):
                action = Action(a, a)
                allowed_actions.append(action)

            state.add_allowed_actions(allowed_actions)
            self.add_state(state)

        self.add_state(broke)
        self.add_state(won)
