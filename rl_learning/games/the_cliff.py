import operator

from rl_learning.common.environment import State, Action
from rl_learning.common.environment import Environment


class TheCliff(Environment):

    def __init__(self):
        super().__init__()

    def transition_reward_function(self, state: State, action: Action) -> (State, float):
        is_terminal = False
        n_state = tuple(map(operator.add, state.name, action.value))
        x = n_state[0]
        y = n_state[1]
        if 12 > x > 1 == y:
            is_terminal = True
            reward = - 100
        elif x == 12 and y == 1:
            is_terminal = True
            reward = -1
        else:
            reward = -1
        next_state = State(n_state, is_terminal)
        for state in self.all_states:
            if state == next_state:
                return state, reward

    def init_environment(self):
        up = Action("UP", (0, 1))
        down = Action("DOWN", (0, -1))
        left = Action("LEFT", (-1, 0))
        right = Action("RIGHT", (1, 0))

        for x in range(1, 13, 1):
            for y in range(1, 5, 1):
                is_terminal = False
                if (x in range(2, 13)) and (y == 1):
                    is_terminal = True
                state = State((x, y), is_terminal)
                allowed_actions = []
                if x > 1:
                    allowed_actions.append(left)
                if x < 12:
                    allowed_actions.append(right)
                if y > 1:
                    allowed_actions.append(down)
                if y < 4:
                    allowed_actions.append(up)
                state.add_allowed_actions(allowed_actions)
                self.add_state(state)
        pass
