import random

from rl_learning.common.environment import State, Action, Environment
from rl_learning.games.base import Game


class Roulette(Game):

    def __init__(self, final_state=20, payouts: list = None):
        if payouts is None:
            payouts = [1, 2, 5, 8, 11, 17, 35]
        self.final_state_value = final_state
        self.payouts = payouts

    def transition_reward_function(self, state: State, action: Action) -> (State, Action):
        payout = action.name
        p_win = 1. / (payout + (1 + payout) / self.payouts[-1])
        result = random.random()
        if result < p_win:
            next_state_value = state.name + action.value * payout
        else:
            next_state_value = state.name - action.value

        reward = 0
        is_terminal = False
        if next_state_value >= self.final_state_value:
            reward = 1
            is_terminal = True
        elif next_state_value == 0:
            is_terminal = True

        next_state = State(next_state_value, next_state_value, is_terminal)
        allowed_actions = []
        if not is_terminal:
            for j in range(0, len(self.payouts)):
                if (j > 0 and self.final_state_value - next_state_value <= self.payouts[j - 1]):
                    continue
                for a in range(1, min(next_state_value, (self.final_state_value - next_state_value) * self.payouts[j]) + 1):
                    action = Action(self.payouts[j], a)
                    allowed_actions.append(action)
        next_state.add_allowed_actions(allowed_actions)

        return next_state, reward

    def init_environment(self) -> Environment:
        final_state = self.final_state_value
        environment = Environment("Gambler ruin", self.transition_reward_function)
        broke = State(0, 0, True)
        won = State(final_state, final_state, True)

        for i in range(1, final_state):
            state = State(i, i, False)
            allowed_actions = []
            for j in range(0, len(self.payouts)):
                if(j > 0 and final_state - i <= self.payouts[j - 1]):
                    continue
                for a in range(1, min(i, (final_state - i) * self.payouts[j]) + 1):
                    if(i + (a - 1) * self.payouts[j] >= final_state):
                        break
                    action = Action(self.payouts[j], a)
                    allowed_actions.append(action)
            state.add_allowed_actions(allowed_actions)
            environment.add_state(state)

        environment.add_state(broke)
        environment.add_state(won)

        return environment
