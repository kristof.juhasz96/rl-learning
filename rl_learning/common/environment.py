import random
from abc import ABC, abstractmethod
from typing import List, Dict, Optional, Tuple


class Action:

    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __eq__(self, other):
        return other.name == self.name

    def __hash__(self):
        return self.name.__hash__()

    def __repr__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)


class State:

    def __init__(self, name, is_terminal: False):
        self.name = name
        self.is_terminal = is_terminal
        self.allowed_actions: List = []
        self.action_values: Dict = {}

    def __hash__(self):
        return self.name.__hash__()

    def __eq__(self, other):
        return (self.name == other.name) and (self.is_terminal == other.is_terminal)

    def __repr__(self):
        return str(self.name)

    def __str__(self):
        return str(self.name)

    def add_allowed_actions(self, allowed_actions: List[Action]):
        self.allowed_actions = allowed_actions

    def add_transition_probabilities(self, transition_probabilities: Dict[Action, Dict[object, float]]):
        self.transition_probabilites = transition_probabilities

    def add_reward_probabilities(self, reward_probabilities: Dict[Action, Dict]):
        self.reward_probabilities = reward_probabilities


class Environment(ABC):

    @abstractmethod
    def transition_reward_function(self, state: State, action: Action) -> (State, float):
        raise NotImplementedError

    @abstractmethod
    def init_environment(self):
        raise NotImplementedError

    def __init__(self, discount_rate=1):

        self.discount_rate = discount_rate
        self.env_name = "None"
        self.episode_count = 1
        self.all_states = []
        self.all_actions = []
        self.current_action_id = 0
        self.current_state_id = 0
        self.current_state: Optional[State] = None
        self.init_environment()

    def reset_episode_count(self):
        self.episode_count = 1

    def set_state(self, state: State):
        self.current_state = state

    def transition_state(self, action):
        if action not in self.current_state.allowed_actions:
            raise ValueError("Not an allowed action")
        else:
            next_state, reward = self.transition_reward_function(self.current_state, action)
        if next_state.is_terminal:
            self.episode_count += 1
        self.current_state = next_state
        return next_state, reward

    def add_state(self, state):
        self.all_states.append(state)

    def reset_starting_state(self, starting_method="random", starting_state=None):
        if starting_method == "random":
            allowed_states = [state for state in self.all_states if not state.is_terminal]
            starting_state = random.choice(allowed_states)

        if starting_method == "state":
            if starting_state is None:
                raise ValueError("You must provide a starting state")

            if starting_state not in self.all_states:
                raise ValueError("Starting state provided not in environment states")

            else:
                for state in self.all_states:
                    if state.name == starting_state.name:
                        starting_state = state

        self.set_state(starting_state)


class Policy(ABC):

    @abstractmethod
    def get_action(self, current_state: State) -> Action:
        raise NotImplementedError

    @abstractmethod
    def get_action_probability(self, action: Action, state: State) -> float:
        raise NotImplementedError


class Simulator:

    def __init__(self, environment: Environment, policy: Policy):
        self.environment = environment
        self.policy = policy
        self.episode_count = 0
        self.episode_rewards = {0: []}

    def make_one_transition(self, action=None):
        next_action = self.policy.get_action(self.environment.current_state)
        next_state, reward = self.environment.transition_state(action=next_action)
        if next_state.is_terminal:
            self.episode_count += 1
            self.episode_rewards[self.episode_count] = []
        self.episode_rewards[self.episode_count].append(reward)
        return next_action, reward, next_state


    def get_episode(self, starting_method="random", starting_state=None) -> Tuple[
        List[State], List[Action], List[float]]:
        state_list = []
        action_list = []
        reward_list = []

        self.environment.reset_starting_state(starting_method=starting_method, starting_state=starting_state)
        state_list.append(self.environment.current_state)
        while not self.environment.current_state.is_terminal:
            next_action, reward, next_state = self.make_one_transition()
            action_list.append(next_action)
            if not next_state.is_terminal:
                state_list.append(next_state)
            reward_list.append(reward)
        return state_list, action_list, reward_list
