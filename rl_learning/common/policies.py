import random

from rl_learning.common.environment import Policy, State, Action, Environment
from rl_learning.common.environment import State, Action

class DecayEGreedyPolicy(Policy):

    def __init__(self, estimated_action_values, environment: Environment):

        self.environment = environment
        self.estimated_action_values = estimated_action_values

    def get_action(self, current_state: State) -> Action:
        epsilon = 1 / (self.environment.episode_count)
        result = random.random()
        if result < epsilon:
            actions_to_pick = list(self.estimated_action_values[current_state].keys())
            action_chosen = random.choice(actions_to_pick)
        else:
            possible_actions = self.estimated_action_values[current_state]
            # max_key = max(possible_actions, key=possible_actions.get)
            # return max_key
            max_value = max(list(possible_actions.values()))
            maximizing_actions = []
            for key, value in possible_actions.items():
                if possible_actions[key] == max_value:
                    maximizing_actions.append(key)
            action_chosen = random.choice(maximizing_actions)
        return action_chosen

    def get_action_probability(self, action: Action, state: State) -> float:
        epsilon = 1 / self.environment.episode_count
        actions = list(self.estimated_action_values[state].keys())
        possible_actions = self.estimated_action_values[state]
        max_value = max(list(possible_actions.values()))
        maximizing_actions = []
        non_maximizing_actions = []
        for key, value in possible_actions.items():
            if possible_actions[key] == max_value:
                maximizing_actions.append(key)
            else:
                non_maximizing_actions.append(key)

        act_len = len(actions)

        if action in maximizing_actions:
            return (1 - epsilon) * (1 / len(maximizing_actions)) + epsilon * (1 / len(actions))

        else:
            return epsilon * (1 / len(actions))


class DecayDoubleEgreedy(Policy):

    def __init__(self, estimated_action_values_1, estimated_action_values_2, environment: Environment):

        self.environment = environment
        self.estimated_action_values_1 = estimated_action_values_1
        self.estimated_action_values_2 = estimated_action_values_2

    def get_action(self, current_state: State) -> Action:
        epsilon = 1 / self.environment.episode_count
        result = random.random()
        sum_estimated_action_values = {}
        for action in self.estimated_action_values_1[current_state]:
            sum_estimated_action_values[action] = self.estimated_action_values_1[current_state][action] + \
                                                  self.estimated_action_values_2[current_state][action]

        if result < epsilon:
            actions_to_pick = list(sum_estimated_action_values.keys())
            action_chosen = random.choice(actions_to_pick)
        else:
            possible_actions = sum_estimated_action_values
            max_value = max(list(possible_actions.values()))
            maximizing_actions = []
            for key, value in possible_actions.items():
                if possible_actions[key] == max_value:
                    maximizing_actions.append(key)
            action_chosen = random.choice(maximizing_actions)
        return action_chosen

    def get_action_probability(self, action: Action, state: State) -> float:
        pass


class EGreedyPolicy(Policy):

    def __init__(self, estimated_action_values, epsilon=0.1):

        self.epsilon = epsilon
        self.estimated_action_values = estimated_action_values

    def get_action(self, current_state: State) -> Action:

        result = random.random()
        if result < self.epsilon:
            actions_to_pick = list(self.estimated_action_values[current_state].keys())
            action_chosen = random.choice(actions_to_pick)
        else:
            possible_actions = self.estimated_action_values[current_state]
            # max_key = max(possible_actions, key=possible_actions.get)
            # return max_key
            max_value = max(list(possible_actions.values()))
            maximizing_actions = []
            for key, value in possible_actions.items():
                if possible_actions[key] == max_value:
                    maximizing_actions.append(key)
            action_chosen = random.choice(maximizing_actions)
        return action_chosen

    def get_action_probability(self, action: Action, state: State) -> float:
        actions = list(self.estimated_action_values[state].keys())
        possible_actions = self.estimated_action_values[state]
        max_value = max(list(possible_actions.values()))
        maximizing_actions = []
        non_maximizing_actions = []
        for key, value in possible_actions.items():
            if possible_actions[key] == max_value:
                maximizing_actions.append(key)
            else:
                non_maximizing_actions.append(key)

        act_len = len(actions)

        if action in maximizing_actions:
            return (1 - self.epsilon) * (1 / len(maximizing_actions)) + self.epsilon * (1 / len(actions))

        else:
            return self.epsilon * (1 / len(actions))


class DoubleEGreedyPolicy(Policy):

    def __init__(self, estimated_action_values_1, estimated_action_values_2, epsilon=0.1):

        self.epsilon = epsilon
        self.estimated_action_values_1 = estimated_action_values_1
        self.estimated_action_values_2 = estimated_action_values_2

    def get_action(self, current_state: State) -> Action:
        result = random.random()
        sum_estimated_action_values = {}
        for action in self.estimated_action_values_1[current_state]:
            sum_estimated_action_values[action] = self.estimated_action_values_1[current_state][action] + \
                                                  self.estimated_action_values_2[current_state][action]

        if result < self.epsilon:
            actions_to_pick = list(sum_estimated_action_values.keys())
            action_chosen = random.choice(actions_to_pick)
        else:
            possible_actions = sum_estimated_action_values
            max_value = max(list(possible_actions.values()))
            maximizing_actions = []
            for key, value in possible_actions.items():
                if possible_actions[key] == max_value:
                    maximizing_actions.append(key)
            action_chosen = random.choice(maximizing_actions)
        return action_chosen

    def get_action_probability(self, action: Action, state: State) -> float:
        pass


class SimpleSoftPolicy(Policy):

    def get_action(self, current_state: State) -> Action:
        actions = current_state.allowed_actions
        random_action = random.choice(actions)
        return random_action

    def get_action_probability(self, action: Action, state: State) -> float:
        actions = state.allowed_actions
        return 1 / len(actions)
