from rl_learning.common.environment import Simulator, Environment
from rl_learning.common.policies import SimpleSoftPolicy
from rl_learning.policy_algorithms.base import PolicyAlgoBase


class RandomAlgo(PolicyAlgoBase):

    def __init__(self, environment: Environment):

        super().__init__(environment)
        self.algo_name = "Random Algo"
        self.simulator: Simulator = None
        self.environment = environment
        self.estimated_action_values = self.init_state_action_dict()
        self.estimated_optimal_policy = {}
        for state in self.environment.all_states:
            self.update_estimated_policy_for_state(state)

        self.simulator = Simulator(environment=environment, policy=SimpleSoftPolicy())

    def main_loop(self, number_episodes=500000, starting_method="random"):
        self.environment.reset_episode_count()
        current_episode = 0
        while current_episode < number_episodes:
            current_episode += 1
            states, actions, rewards = self.simulator.get_episode(starting_method=starting_method)
