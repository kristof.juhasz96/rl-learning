import random

from rl_learning.common.environment import Simulator, Environment
from rl_learning.common.policies import DecayDoubleEgreedy, DoubleEGreedyPolicy
from rl_learning.policy_algorithms.base import PolicyAlgoBase


class DoubleQLearning(PolicyAlgoBase):

    def __init__(self, environment: Environment, config):
        super().__init__(environment, config=config)
        self.algo_name = "Double Q learning"
        self.step_size = config["step_size"]
        self.epsilon = config["epsilon"]

        self.estimated_action_values_1 = self.init_state_action_dict(0.5)
        self.estimated_action_values_2 = self.init_state_action_dict(0.5)
        if config["behavior_policy"] == "DoubleEgreedy":
            self.behaviour_policy = DoubleEGreedyPolicy(self.estimated_action_values_1, self.estimated_action_values_2,
                                                        epsilon=self.epsilon)

        elif config["behavior_policy"] == "DecayDoubleEgreedy":
            self.behaviour_policy = DecayDoubleEgreedy(self.estimated_action_values_1, self.estimated_action_values_2,
                                                       environment=environment)
        self.simulator = Simulator(environment=environment, policy=self.behaviour_policy)

    def main_loop(self, number_episodes=1000, starting_method="random", starting_state=None):
        self.environment.reset_episode_count()
        current_episode = 0
        while current_episode < number_episodes:
            current_episode += 1

            self.environment.reset_starting_state(starting_method=starting_method, starting_state=starting_state)
            while not self.environment.current_state.is_terminal:
                current_state = self.environment.current_state
                next_action, reward, next_state = self.simulator.make_one_transition()
                coin_flip = random.random()
                if coin_flip < 0.5:
                    to_update_estimate = self.estimated_action_values_1
                    other_estimate = self.estimated_action_values_2

                else:

                    to_update_estimate = self.estimated_action_values_2
                    other_estimate = self.estimated_action_values_1

                current_value = to_update_estimate[current_state][next_action]


                possible_actions = to_update_estimate[next_state]
                max_value = max(list(possible_actions.values()))
                maximizing_actions = []
                for key, value in possible_actions.items():
                    if possible_actions[key] == max_value:
                        maximizing_actions.append(key)
                action_chosen = random.choice(maximizing_actions)

                max_action_value = other_estimate[next_state][action_chosen]
                updated_value = current_value + self.step_size * (
                        reward + self.environment.discount_rate * max_action_value - current_value)
                to_update_estimate[current_state][next_action] = updated_value

    def get_estimated_optimal_policy(self):
        for key, item in self.estimated_optimal_policy.items():
            if item is not None:
                print(f" {key.name} ----> {item.name}\n")

    def get_action_values(self):
        state_x = []
        value_y = []
        for state in self.estimated_action_values_1.keys():
            if not state.is_terminal:
                state_values = []
                for action in state.allowed_actions:
                    state_values.append(
                        (self.estimated_action_values_1[state][action] + self.estimated_action_values_2[state][
                            action]) / 2)
                if len(state_values) > 0:
                    value_y.append(max(state_values))
                    state_x.append(state.name)

        return state_x, value_y
