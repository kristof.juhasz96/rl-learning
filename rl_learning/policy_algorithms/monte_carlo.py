from rl_learning.common.environment import Environment
from rl_learning.common.environment import Simulator
from rl_learning.common.policies import EGreedyPolicy
from rl_learning.common.policies import DecayEGreedyPolicy
from rl_learning.policy_algorithms.base import PolicyAlgoBase


class OffPolicyMonteCarlo(PolicyAlgoBase):

    def __init__(self, environment: Environment, config):

        super().__init__(environment, config)
        self.algo_name = "Off Policy Monte Carlo"
        self.epsilon = config["epsilon"]
        self.cumulative_sums = self.init_state_action_dict(init_value=0)
        if config["behavior_policy"] == "Egreedy":
            self.behaviour_policy = EGreedyPolicy(self.estimated_action_values,
                                                  epsilon=self.epsilon)

        elif config["behavior_policy"] == "DecayEgreedy":
            self.behaviour_policy = DecayEGreedyPolicy(self.estimated_action_values,
                                                       environment=environment)
        self.simulator = Simulator(environment=environment, policy=self.behaviour_policy)

    def main_loop(self, number_episodes=500000, starting_method="random"):
        self.environment.reset_episode_count()
        current_episode = 0
        while current_episode < number_episodes:
            current_episode += 1
            states, actions, rewards = self.simulator.get_episode(starting_method=starting_method)
            action_behaviour_probabilities = []
            for j in range(len(states)):
                action_behaviour_probabilities.append(
                    self.behaviour_policy.get_action_probability(actions[j], states[j]))

            G = 0
            W = 1
            indices = list(range(len(rewards) - 1, -1, -1))
            for i in indices:
                state_n = states[i]
                action_n = actions[i]
                reward_n = rewards[i]
                action_prob = action_behaviour_probabilities[i]

                G = self.environment.discount_rate * G + reward_n

                old_cum_sum = self.cumulative_sums[state_n][action_n]
                cum_sum_update = old_cum_sum + W
                # _ = self.behaviour_policy.get_action_probability(action_n, state_n)
                old_estimate_value = self.estimated_action_values[state_n][action_n]
                estimated_value_update = old_estimate_value + (
                        W / cum_sum_update) * (G - old_estimate_value)

                self.cumulative_sums[state_n][action_n] = cum_sum_update
                self.estimated_action_values[state_n][action_n] = estimated_value_update

                self.update_estimated_policy_for_state(state_n)
                action_chosen = self.estimated_optimal_policy[state_n]

                if action_n != action_chosen:
                    break

                action_prob_2 = self.behaviour_policy.get_action_probability(action_n, state_n)
                W = W * (1 / self.behaviour_policy.get_action_probability(action_n, state_n))
                # W = W * (1 / action_prob)
