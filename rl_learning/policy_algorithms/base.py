import random
from abc import ABC, abstractmethod
from typing import Dict

import numpy as np
from matplotlib import pyplot as plt

from rl_learning.common.environment import State, Action, Simulator, Environment


def moving_average(a, n):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n


class PolicyAlgoBase(ABC):

    def __init__(self, environment: Environment, config={}):

        self.config = config
        self.algo_name = "None"
        self.simulator: Simulator = None
        self.environment = environment
        self.estimated_action_values = self.init_state_action_dict()
        self.estimated_optimal_policy = {}
        for state in self.environment.all_states:
            self.update_estimated_policy_for_state(state)

    @abstractmethod
    def main_loop(self):
        raise NotImplementedError

    def update_estimated_policy_for_state(self, state: State):
        self.estimated_optimal_policy[state] = self.get_max_action_for_state(state)

    def get_max_action_for_state(self, state: State):
        values = self.estimated_action_values[state]
        if len(values.keys()) == 0:
            return None
        else:
            possible_actions = self.estimated_action_values[state]
            max_value = max(list(possible_actions.values()))
            maximizing_actions = []
            for key, value in possible_actions.items():
                if possible_actions[key] == max_value:
                    maximizing_actions.append(key)
            return random.choice(maximizing_actions)

            # possible_actions = self.estimated_action_values[state]
            # max_key = max(possible_actions, key=possible_actions.get)
            # return max_key

    def init_state_action_dict(self, init_value=0.5):
        return_dict: Dict = {}
        for state in self.environment.all_states:
            return_dict[state] = {}
            if state.is_terminal:
                return_dict[state] = {Action("No action", 0): 0}
            else:
                for action in state.allowed_actions:
                    return_dict[state][action] = init_value
        return return_dict

    def get_estimated_optimal_policy(self):
        for key, item in self.estimated_optimal_policy.items():
            if item is not None:
                print(f" {key.name} ----> {item.name}\n")

    def get_action_values(self):
        value_function = self.estimated_action_values
        state_x = []
        value_y = []
        for state in value_function.keys():
            if not state.is_terminal:
                values = value_function[state].values()
                if len(values) > 0:
                    value_y.append(max(values))
                    state_x.append(str(state.name))

        return state_x, value_y

    def plot_action_values(self):
        state_x, value_y = self.get_action_values()
        fig = plt.figure(figsize=(15, 5))
        axs = fig.add_axes([0.1, 0.1, 0.8, 0.8])
        axs.set_title("Estimated maximum state values")
        axs.set_ylabel("State ID-s")
        axs.plot(state_x, value_y)
        axs.grid()

    def get_rewards_stats(self):
        episode_rewards = self.simulator.episode_rewards
        episode_sums = []

        for episode, rewards in episode_rewards.items():
            rews = np.array(rewards)
            episode_sum = np.sum(rews)
            episode_sums.append(episode_sum)

        episodes_cum_sum = np.cumsum(episode_sums)
        moving_mean = moving_average(episode_sums, 500)
        return episodes_cum_sum, moving_mean

    def plot_rewards(self):
        episodes_cum_sum, moving_mean = self.get_rewards_stats()
        fig, axs = plt.subplots(3, figsize=(20, 20))
        axs[0].plot(moving_mean)
        axs[0].set_title("Total sum of episodes")
        axs[0].grid()

        axs[1].plot(episodes_cum_sum)
        axs[1].grid()
        axs[1].set_title("Cumulative sum of episodes")
