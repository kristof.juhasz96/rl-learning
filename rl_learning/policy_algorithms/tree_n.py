import random

import matplotlib.pyplot as plt
import numpy as np

from rl_learning.common.environment import Environment
from rl_learning.common.environment import Simulator
from rl_learning.common.policies import EGreedyPolicy
from rl_learning.common.policies import DecayEGreedyPolicy
from rl_learning.policy_algorithms.base import PolicyAlgoBase


class TreeN(PolicyAlgoBase):

    def __init__(self, environment: Environment, config):
        super().__init__(environment, config)
        self.algo_name = "Tree N"
        self.epsilon = config["epsilon"]
        self.cumulative_sums = self.init_state_action_dict(init_value=0)
        if config["behavior_policy"] == "Egreedy":
            self.behaviour_policy = EGreedyPolicy(self.estimated_action_values,
                                                  epsilon=self.epsilon)

        elif config["behavior_policy"] == "DecayEgreedy":
            self.behaviour_policy = DecayEGreedyPolicy(self.estimated_action_values,
                                                       environment=environment)
        self.simulator = Simulator(environment=environment, policy=self.behaviour_policy)
        self.environment.discount_rate = config["discount_rate"]
        self.step_size = config["step_size"]
        self.N = config['N']

    def main_loop(self, starting_method="random", number_episodes=1000,  starting_state=None):
        gamma = self.environment.discount_rate
        current_episode = 0
        self.environment.reset_episode_count()
        while current_episode < number_episodes:
            current_episode += 1
            self.environment.reset_starting_state(starting_method=starting_method, starting_state=starting_state)

            state_list = list(0 for i in range(self.N + 1))
            action_list = list(0 for i in range(self.N + 1))
            reward_list = list(0 for i in range(self.N + 1))

            state_list[-1] = self.environment.current_state
            action, reward, next_state = self.simulator.make_one_transition()
            self.environment.current_state = next_state
            state_list = state_list[1:]
            state_list.append(next_state)
            reward_list = reward_list[1:]
            reward_list.append(reward)
            action_list = action_list[1:]
            action_list.append(action)

            T = np.inf
            t = 0
            while(t < T):
                if not state_list[-1].is_terminal:
                    action, reward, next_state = self.simulator.make_one_transition()
                    self.environment.current_state = next_state
                else:
                    T = t + 1
                action_list = action_list[1:]
                action_list.append(action)

                tau = t - self.N + 1
                if (tau >= 0):
                    if next_state.is_terminal:
                        G = reward_list[-1]
                    else:
                        G = reward_list[-1]
                        possible_actions = list(self.estimated_action_values[state_list[-1]].keys())
                        for action in possible_actions:
                            pi = self.behaviour_policy.get_action_probability(action, state_list[-1])
                            q = self.estimated_action_values[state_list[-1]][action]
                            G = G + pi * q * gamma

                    for k in range(-2, -self.N - 1, -1):
                        G = reward_list[k] + gamma * self.behaviour_policy.get_action_probability(action_list[k],
                                                                                                  state_list[k]) * G
                        possible_actions = list(self.estimated_action_values[state_list[k]].keys())
                        for action in possible_actions:
                            if(action == action_list[k]):
                                continue
                            pi = self.behaviour_policy.get_action_probability(action, state_list[k])
                            q = self.estimated_action_values[state_list[k]][action]
                            G = G + pi * q * self.environment.discount_rate

                    to_update_state = state_list[0]
                    to_update_action = action_list[0]
                    to_update_estimate = self.estimated_action_values[to_update_state][to_update_action]
                    self.estimated_action_values[to_update_state][
                        to_update_action] = to_update_estimate + self.step_size * (G - to_update_estimate)

                t = t + 1

                if(t != T):
                    state_list = state_list[1:]
                    state_list.append(next_state)
                    reward_list = reward_list[1:]
                    reward_list.append(reward)

            for i in range(1, min(self.N, T + 1)):
                G = reward_list[-1]
                for k in range(-self.N - 1 + i, -self.N, -1):
                    G = reward_list[k] + gamma * self.behaviour_policy.get_action_probability(action_list[k],
                                                                                              state_list[k]) * G
                    possible_actions = list(self.estimated_action_values[state_list[k]].keys())
                    for action in possible_actions:
                        if (action == action_list[k]):
                            continue
                        pi = self.behaviour_policy.get_action_probability(action, state_list[k])
                        q = self.estimated_action_values[state_list[k]][action]
                        G = G + pi * q * self.environment.discount_rate

                to_update_state = state_list[-(i + 1)]
                to_update_action = action_list[-(i + 1)]
                to_update_estimate = self.estimated_action_values[to_update_state][to_update_action]
                self.estimated_action_values[to_update_state][
                    to_update_action] = to_update_estimate + self.step_size * (G - to_update_estimate)

    def get_estimated_optimal_policy(self):
        for key, item in self.estimated_optimal_policy.items():
            if item is not None:
                print(f" {key.name} ----> {item.name}\n")

