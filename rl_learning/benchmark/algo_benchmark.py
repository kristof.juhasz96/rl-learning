import matplotlib.pyplot as plt

from rl_learning.policy_algorithms.random_algo import RandomAlgo
from rl_learning.common.environment import Environment


class AlgoBenchmark:

    def __init__(self, algo_list, environment: Environment, number_episodes):
        self.algo_list = algo_list
        random_algo = RandomAlgo(environment=environment)
        random_algo.main_loop(starting_method="random", number_episodes=number_episodes)
        self.algo_list.append(random_algo)

    def plot_benchmarks(self):
        fig, axs = plt.subplots(3, figsize=(20, 20))
        for algo in self.algo_list:
            episodes_cum_sum, moving_mean = algo.get_rewards_stats()
            state_x, value_y = algo.get_action_values()

            axs[0].plot(moving_mean, label=f"{algo.algo_name}:  {algo.config}")
            axs[1].plot(episodes_cum_sum, label=f"{algo.algo_name}:  {algo.config}")
            axs[2].plot(state_x, value_y, label=f"{algo.algo_name}:  {algo.config}")

        axs[1].set_title("Cumulative sum of episodes")
        axs[1].grid()
        axs[1].legend()

        axs[0].set_title("Moving mean of episode rewards")
        axs[0].grid()
        axs[0].legend()

        axs[2].set_title("Estimated maximum state values")
        axs[2].grid()
        axs[2].legend()
